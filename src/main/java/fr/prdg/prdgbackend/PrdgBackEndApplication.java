package fr.prdg.prdgbackend;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@lombok.Generated
public class PrdgBackEndApplication {

	public static void main(String[] args) {
		SpringApplication.run(PrdgBackEndApplication.class, args);
	}

}
